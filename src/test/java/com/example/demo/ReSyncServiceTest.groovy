package com.example.demo

import org.springframework.amqp.rabbit.core.RabbitTemplate
import spock.lang.Specification

class ReSyncServiceTest extends Specification {

  def "should resend single event to resource sync queue" () {
    given:
    String str = 'someValue'
    def rabbitTemplate = Mock(RabbitTemplate){
      receiveAndConvert("FOO") >> { str }
    }
    ReSyncService reSyncService = new ReSyncService(rabbitTemplate)

    when:
    reSyncService.retry()

    then:
    1 * rabbitTemplate.convertAndSend("BAR", _ as String) >> {
      arguments ->
        assert "BAR" == arguments[0]
        assert str == arguments[1]
    }

  }

}
