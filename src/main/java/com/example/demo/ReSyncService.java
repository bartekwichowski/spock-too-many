package com.example.demo;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class ReSyncService {

  private final RabbitTemplate rabbitTemplate;

  public void retry() {
    String foo;
    while (rabbitTemplate != null && (foo = (String) rabbitTemplate.receiveAndConvert("FOO")) != null) {
      rabbitTemplate.convertAndSend("BAR", foo);
    }
  }

}
